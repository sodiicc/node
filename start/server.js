// let User = require('./user');
import {User} from './user'

let log = require('./logger')(module);
let db = require('./db')

db.connect()

function run() {
  let vasya = new User('Vasia');
  let petya = new User('Petya');

  vasya.hello(petya)
  log(db.getPhrase('RUN SUCCESSFUL'))
}

if (module.parent) {
  exports.run = run
} else {
  run()
}