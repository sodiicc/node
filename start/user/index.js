let db = require('../db')

let log = require('../logger')(module);
// console.log('log', log)

class User {
  constructor(name) {
    this.name = name;
  }
  hello(who){
      log(db.getPhrase("hello") + ', '+ who.name );
  };
};

log('user.js is REQUIRED!')

// module.exports = User
export {User}