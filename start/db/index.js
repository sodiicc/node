let phrases;

exports.connect = function(){
  phrases = require('./ukr')
}

exports.getPhrase = function(name){
  if(!phrases[name]){
    throw new Error('Такої фрази ен існує: ' + name);
  }
  return phrases[name]
}